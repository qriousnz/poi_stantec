library(ggmap)
library(leaflet)
library(plyr)
library(dplyr)
library(magrittr)
library(sqldf)

filePath <-"~/Desktop/test.csv"

oRawData <- read.csv(filePath)
oDataset<-sqldf('select * from oRawData where 1=1')
  
MBU <- "https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoicXJpb3VzaXZhbiIsImEiOiJjaXB4aGcxZmswd3J2ZmptMjdjbnIzeGJsIn0.iQSktg-Tm579n_uxZHe_Ig"
lf_obj <- leaflet() %>%
  addTiles(urlTemplate = MBU) %>%
  addMarkers(oDataset$long, 
             oDataset$lat,
             popup = paste0("cell id rti: ", oDataset$cell_id_rti, 
                            " Grid12: ", oDataset$grid12,
                            " Entrance: ", oDataset$entrance,
                            " Start Date: ", oDataset$start_date_str))
lf_obj
